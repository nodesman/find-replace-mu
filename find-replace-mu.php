<?php
/*
Plugin Name: Find Replace MU
Description: Plugin to find and replace text thoughout the entire WordPress network
Author: Raj Sekharan
Author URI: http://nodesman.com
*/

$current_dir = dirname(__FILE__);

$dir = $current_dir;
define("FR_PATH",$dir);

require FR_PATH. "/crons.php";
require FR_PATH. "/menu.php";
require FR_PATH. "/interface.php";

add_action("init","_fr_init");

function _fr_init()
{
     add_action("admin_menu" , "_fr_menu");
}

